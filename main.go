package main

import (
	"bufio"
	"io"
	"log"
	"os"
	"unicode/utf8"
)

const (
	filenameRes = "results.txt"
	m = 66
	a = 5
	c = 7
	numberSymbols = 66
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* encode and write text using "mod" method */
func encodeTextMod(filename string) {

	/* declaration of variables */
	var (
		err        error
		reader     *bufio.Reader
		fileInput  *os.File
		fileOutput *os.File
		line       []byte
		row        string
		newRow     string
		width, w   int
		i, i1      int
		symbol     int32
		hashTable  map[int]int32
	)

	/* open file for reading */
	fileInput, err = os.Open(filename)
	handleError(err)

	/* create file to write results */
	fileOutput, err = os.Create(filenameRes)
	handleError(err)

	/* create numeration of symbols */
	hashTable = make(map[int]rune)
	hashTable[0] = 'А'
	hashTable[1] = 'a'
	hashTable[2] = 'Б'
	hashTable[3] = 'б'
	hashTable[4] = 'В'
	hashTable[5] = 'в'
	hashTable[6] = 'Г'
	hashTable[7] = 'г'
	hashTable[8] = 'Ґ'
	hashTable[9] = 'ґ'
	hashTable[10] = 'Д'
	hashTable[11] = 'д'
	hashTable[12] = 'Е'
	hashTable[13] = 'е'
	hashTable[14] = 'Є'
	hashTable[15] = 'є'
	hashTable[16] = 'Ж'
	hashTable[17] = 'ж'
	hashTable[18] = 'З'
	hashTable[19] = 'з'
	hashTable[20] = 'И'
	hashTable[21] = 'и'
	hashTable[22] = 'І'
	hashTable[23] = 'і'
	hashTable[24] = 'Ї'
	hashTable[25] = 'ї'
	hashTable[26] = 'Й'
	hashTable[27] = 'й'
	hashTable[28] = 'К'
	hashTable[29] = 'к'
	hashTable[30] = 'Л'
	hashTable[31] = 'л'
	hashTable[32] = 'М'
	hashTable[33] = 'м'
	hashTable[34] = 'Н'
	hashTable[35] = 'н'
	hashTable[36] = 'О'
	hashTable[37] = 'о'
	hashTable[38] = 'П'
	hashTable[39] = 'п'
	hashTable[40] = 'Р'
	hashTable[41] = 'р'
	hashTable[42] = 'С'
	hashTable[43] = 'с'
	hashTable[44] = 'Т'
	hashTable[45] = 'т'
	hashTable[46] = 'У'
	hashTable[47] = 'у'
	hashTable[48] = 'Ф'
	hashTable[49] = 'ф'
	hashTable[50] = 'Х'
	hashTable[51] = 'х'
	hashTable[52] = 'Ц'
	hashTable[53] = 'ц'
	hashTable[54] = 'Ч'
	hashTable[55] = 'ч'
	hashTable[56] = 'Ш'
	hashTable[57] = 'ш'
	hashTable[58] = 'Щ'
	hashTable[59] = 'щ'
	hashTable[60] = 'Ь'
	hashTable[61] = 'ь'
	hashTable[62] = 'Ю'
	hashTable[63] = 'ю'
	hashTable[64] = 'Я'
	hashTable[65] = 'я'

	/* create reader and read file */
	reader = bufio.NewReader(fileInput)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		newRow = ""
		/* handle row */
		w = 0
		for i = 0; i < len(row); i += w {
			/* handle the symbol (encode) */
			symbol, width = utf8.DecodeRuneInString(row[i:])
			/* find symbol in hash table and apply affine cipher */
			i1 = 0
			for i1 < numberSymbols {
				if hashTable[i1] == symbol {
					symbol = hashTable[(a * i1 + c) % m]
					break
				}
				i1++
			}
			newRow = newRow + string(symbol)
			w = width
		}

		/* write final result */
		_, err = fileOutput.WriteString(newRow + "\n")
		handleError(err)

	}

	/* close files */
	err = fileInput.Close()
	handleError(err)
	err = fileOutput.Close()
	handleError(err)

}

/* encode and write text using "sub" method */
func encodeTextSub(filename string) {

	var (
		encodeSub map[int32]int32
		err        error
		reader     *bufio.Reader
		fileInput  *os.File
		fileOutput *os.File
		line       []byte
		row        string
		newRow     string
		width, w   int
		i          int
		symbol     int32
		newSymbol  int32
	)

	encodeSub = make(map[int32]int32)
	encodeSub['A'] = 'Й'
	encodeSub['a'] = 'й'
	encodeSub['Б'] = 'Ґ'
	encodeSub['б'] = 'ґ'
	encodeSub['В'] = 'П'
	encodeSub['в'] = 'п'
	encodeSub['Г'] = 'А'
	encodeSub['г'] = 'а'
	encodeSub['Ґ'] = 'Д'
	encodeSub['ґ'] = 'д'
	encodeSub['Е'] = 'Б'
	encodeSub['е'] = 'б'
	encodeSub['Є'] = 'К'
	encodeSub['є'] = 'к'
	encodeSub['Ж'] = 'Е'
	encodeSub['ж'] = 'е'
	encodeSub['З'] = 'Ц'
	encodeSub['з'] = 'ц'
	encodeSub['И'] = 'Є'
	encodeSub['и'] = 'є'
	encodeSub['І'] = 'Ф'
	encodeSub['і'] = 'ф'
	encodeSub['Ї'] = 'М'
	encodeSub['ї'] = 'м'
	encodeSub['Й'] = 'Ю'
	encodeSub['й'] = 'ю'
	encodeSub['К'] = 'Ж'
	encodeSub['к'] = 'ж'
	encodeSub['Л'] = 'З'
	encodeSub['л'] = 'з'
	encodeSub['М'] = 'І'
	encodeSub['м'] = 'і'
	encodeSub['Н'] = 'Ї'
	encodeSub['н'] = 'ї'
	encodeSub['О'] = 'И'
	encodeSub['о'] = 'и'
	encodeSub['П'] = 'Л'
	encodeSub['п'] = 'л'
	encodeSub['Р'] = 'В'
	encodeSub['р'] = 'в'
	encodeSub['С'] = 'Ш'
	encodeSub['с'] = 'ш'
	encodeSub['Т'] = 'Ч'
	encodeSub['т'] = 'ч'
	encodeSub['У'] = 'Р'
	encodeSub['у'] = 'р'
	encodeSub['Ф'] = 'С'
	encodeSub['ф'] = 'с'
	encodeSub['Х'] = 'Т'
	encodeSub['х'] = 'т'
	encodeSub['Ц'] = 'Н'
	encodeSub['ц'] = 'н'
	encodeSub['Ч'] = 'О'
	encodeSub['ч'] = 'о'
	encodeSub['Ш'] = 'У'
	encodeSub['ш'] = 'у'
	encodeSub['Щ'] = 'Ь'
	encodeSub['щ'] = 'ь'
	encodeSub['Ь'] = 'Щ'
	encodeSub['ь'] = 'щ'
	encodeSub['Ю'] = 'Г'
	encodeSub['ю'] = 'г'
	encodeSub['Я'] = 'Х'
	encodeSub['я'] = 'х'

	/* open file for reading */
	fileInput, err = os.Open(filename)
	handleError(err)

	/* create file to write results */
	fileOutput, err = os.Create(filenameRes)
	handleError(err)

	/* create reader and read file */
	reader = bufio.NewReader(fileInput)
	for {

		/* read one line */
		line, _, err = reader.ReadLine()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}

		/* convert to string */
		row = string(line)
		newRow = ""
		/* handle row */
		w = 0
		for i = 0; i < len(row); i += w {
			/* handle the symbol (encode) */
			symbol, width = utf8.DecodeRuneInString(row[i:])
			if _, ok := encodeSub[symbol]; ok {
				newSymbol = encodeSub[symbol]
			} else {
				newSymbol = symbol
			}
			newRow = newRow + string(newSymbol)
			w = width
		}

		/* write final result */
		_, err = fileOutput.WriteString(newRow + "\n")
		handleError(err)

	}

	/* close files */
	err = fileInput.Close()
	handleError(err)
	err = fileOutput.Close()
	handleError(err)

}

/* entry point */
func main() {

	/* declarations of variables */
	var (
		filename   string
		mode       string
	)

	/* read filename */
	filename = os.Args[1]
	mode = os.Args[2]
	if mode == "mod" {
		encodeTextMod(filename)
	} else if mode == "sub" {
		encodeTextSub(filename)
	}

}